package com.minefx.simplyamazing.Internal;

import com.minefx.simplyamazing.Commands.CommandManager;
import com.minefx.simplyamazing.Events.HandleEvents;
import com.minefx.simplyamazing.Items.ExampleItem;
import com.minefx.simplyamazing.Items.ExampleItem2;
import com.minefx.simplyamazing.Main;
import com.minefx.simplyamazing.Recipes.*;
import org.bukkit.Bukkit;

// Initializes the plugin
public class Init {
    public static void enableMessage(String pluginName) {
        Main.getPlugin(Main.class).getServer().getConsoleSender().sendMessage("[" + pluginName + "] Plugin Enabled!");
    }
    public static void disableMessage(String pluginName) {
        Main.getPlugin(Main.class).getServer().getConsoleSender().sendMessage("[" + pluginName + "] Plugin Disabled!");
    }
    public static void registerEvents(String pluginName) {
        Main.getPlugin(Main.class).getServer().getPluginManager().registerEvents(new HandleEvents(), Main.getPlugin(Main.class));
        Main.getPlugin(Main.class).getServer().getConsoleSender().sendMessage("[" + pluginName + "] Events initialized!");
    }
    public static void loadItems(String pluginName) {
        ExampleItem.init();
        ExampleItem2.init();
        Main.getPlugin(Main.class).getServer().getConsoleSender().sendMessage("[" + pluginName + "] Items initialized!");
    }
    public static void registerCommands(String pluginName) {
        Main.getPlugin(Main.class).getCommand(pluginName).setExecutor(new CommandManager());
        Main.getPlugin(Main.class).getServer().getConsoleSender().sendMessage("[" + pluginName + "] Commands initialized!");
    }
    public static void loadRecipes(String pluginName) {
        ExampleShaped.init();
        ExampleShapeless.init();
        ExampleSmelting.init();
        ExampleSmithing.init();
        ExampleSmoking.init();
        Main.getPlugin(Main.class).getServer().getConsoleSender().sendMessage("[" + pluginName + "] Recipes initialized!");
    }
    public static void unloadRecipes(String pluginName) {
        Bukkit.clearRecipes();
        Main.getPlugin(Main.class).getServer().getConsoleSender().sendMessage("[" + pluginName + "] Unloaded recipes!");
    }
}
