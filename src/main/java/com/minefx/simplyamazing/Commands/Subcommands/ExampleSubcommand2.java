package com.minefx.simplyamazing.Commands.Subcommands;

import com.minefx.simplyamazing.Commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class ExampleSubcommand2 extends SubCommand {

    @Override
    public String getName() {
        return "ExampleCommand2";
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args[3].equalsIgnoreCase("Yes")) {
            Bukkit.broadcastMessage(player + " chose yes!");
        } else if (args[3].equalsIgnoreCase("No")) {
            Bukkit.broadcastMessage(player + "chose no!");
        }
    }

    @Override
    public ArrayList<String> getSubcommandsArgs(Player player, String[] args) {
        ArrayList<String> playerNames = new ArrayList<>();
        if (args.length == 2) {
            playerNames.add("Yes");
            playerNames.add("No");
        }
        return playerNames;
    }
}
