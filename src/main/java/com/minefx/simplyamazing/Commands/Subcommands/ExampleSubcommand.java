package com.minefx.simplyamazing.Commands.Subcommands;

import com.minefx.simplyamazing.Commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class ExampleSubcommand extends SubCommand {
    @Override
    public String getName() {
        return "ExampleCommand";
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public void perform(Player player, String[] args) {
        Bukkit.broadcastMessage("Generic Message");
    }

    @Override
    public ArrayList<String> getSubcommandsArgs(Player player, String[] args) {
        return null;
    }
}
