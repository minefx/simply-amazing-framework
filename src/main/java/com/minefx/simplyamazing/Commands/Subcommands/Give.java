package com.minefx.simplyamazing.Commands.Subcommands;

import com.minefx.simplyamazing.Commands.SubCommand;
import com.minefx.simplyamazing.Events.HandleEvents;
import com.minefx.simplyamazing.Items.ExampleItem;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Give extends SubCommand {

    @Override
    public String getName() {
        return "give";
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public void perform(Player player, String[] args) {
        for (int i = 0; i < HandleEvents.getItemEvents().size(); i++) {
            if (args[1].equalsIgnoreCase(HandleEvents.getItemEvents().get(i).getItem().getItemMeta().getDisplayName().toString())) {
                player.getInventory().addItem(HandleEvents.getItemEvents().get(i).getItem());
            } else {
                player.sendMessage("That item does not exist!");
            }
        }
    }

    @Override
    public ArrayList<String> getSubcommandsArgs(Player player, String[] args) {
        ArrayList<String> list = new ArrayList<>();
        if (args.length == 2) {
            for (int i = 0; i < HandleEvents.getItemEvents().size(); i++) {
                list.add(HandleEvents.getItemEvents().get(i).getItem().getItemMeta().getDisplayName());
            }
        }
        return list;
    }
}
