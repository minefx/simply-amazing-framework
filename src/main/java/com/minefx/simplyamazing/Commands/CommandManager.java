package com.minefx.simplyamazing.Commands;

import com.minefx.simplyamazing.Commands.Subcommands.ExampleSubcommand;
import com.minefx.simplyamazing.Commands.Subcommands.ExampleSubcommand2;
import com.minefx.simplyamazing.Commands.Subcommands.Give;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
//Adapted from https://www.youtube.com/watch?v=rQce_yEXSOE
public class CommandManager implements TabExecutor {
    private ArrayList<SubCommand> subCommands = new ArrayList<>();
    public CommandManager() {
        subCommands.add(new ExampleSubcommand());
        subCommands.add(new ExampleSubcommand2());
        subCommands.add(new Give());
    }
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            for (int i = 0; i < getSubCommands().size(); i++) {
                if (args[0].equalsIgnoreCase(getSubCommands().get(i).getName())) {
                    getSubCommands().get(i).perform(player, args);
                }
            }
        }
        return true;
    }
    public ArrayList<SubCommand> getSubCommands() {
        return subCommands;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 1) {
            ArrayList<String> subCommandArgs = new ArrayList<>();
            for (int i = 0; i < getSubCommands().size(); i++) {
                subCommandArgs.add(getSubCommands().get(i).getName());
            }
            return subCommandArgs;
        } else if (args.length == 2) {
            for (int i = 0; i < getSubCommands().size(); i++) {
                if (args[0].equalsIgnoreCase(getSubCommands().get(i).getName())) {
                    return getSubCommands().get(i).getSubcommandsArgs((Player) sender, args);
                }
            }
        }
        return null;
    }
}
