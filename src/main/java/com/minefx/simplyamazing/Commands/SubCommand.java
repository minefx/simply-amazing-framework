package com.minefx.simplyamazing.Commands;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public abstract class SubCommand {
    public abstract String getName();
    public abstract String getDescription();
    public abstract void perform(Player player, String args[]);
    public abstract ArrayList<String> getSubcommandsArgs(Player player, String args[]);
}
