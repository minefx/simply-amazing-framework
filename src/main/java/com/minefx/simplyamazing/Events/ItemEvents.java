package com.minefx.simplyamazing.Events;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class ItemEvents {
    public abstract ItemStack getItem();
    public abstract void rightClickAir(Player player);
    public abstract void rightClickBlock(Player player);
    public abstract void leftClickAir(Player player);
    public abstract void leftClickBlock(Player player);
}
