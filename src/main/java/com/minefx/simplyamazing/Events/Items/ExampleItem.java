package com.minefx.simplyamazing.Events.Items;

import com.minefx.simplyamazing.Events.ItemEvents;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ExampleItem extends ItemEvents {

    @Override
    public ItemStack getItem() {
        return com.minefx.simplyamazing.Items.ExampleItem.ExampleItem;
    }

    @Override
    public void rightClickAir(Player player) {
        this.leftClickAir(player);
    }

    @Override
    public void rightClickBlock(Player player) {
        this.leftClickAir(player);
    }

    @Override
    public void leftClickAir(Player player) {
        Bukkit.broadcastMessage("This works");
    }

    @Override
    public void leftClickBlock(Player player) {
        this.leftClickAir(player);
    }
}
