package com.minefx.simplyamazing.Events.Items;

import com.minefx.simplyamazing.Events.ItemEvents;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ExampleItem2 extends ItemEvents {
    @Override
    public ItemStack getItem() {
        return com.minefx.simplyamazing.Items.ExampleItem2.ExampleItem2;
    }

    @Override
    public void rightClickAir(Player player) {

    }

    @Override
    public void rightClickBlock(Player player) {

    }

    @Override
    public void leftClickAir(Player player) {

    }

    @Override
    public void leftClickBlock(Player player) {

    }
}
