package com.minefx.simplyamazing.Events;

import com.minefx.simplyamazing.Events.Items.ExampleItem;
import com.minefx.simplyamazing.Events.Items.ExampleItem2;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class HandleEvents implements Listener {
    public static ArrayList<ItemEvents> itemEvents = new ArrayList<ItemEvents>();
    public HandleEvents() {
        itemEvents.add(new ExampleItem());
        itemEvents.add(new ExampleItem2());
    }
    @EventHandler
    public void leftClickAir(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getAction().equals(Action.LEFT_CLICK_AIR)) {
            for (int i = 0; i < getItemEvents().size(); i++) {
                if (event.getItem().getItemMeta().equals(getItemEvents().get(i).getItem().getItemMeta())) {
                    getItemEvents().get(i).leftClickAir(player);
                }
            }
        }
    }
    @EventHandler
    public void rightClickAir(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            for (int i = 0; i < getItemEvents().size(); i++) {
                if (event.getItem().getItemMeta().equals(getItemEvents().get(i).getItem().getItemMeta())) {
                    getItemEvents().get(i).rightClickAir(player);
                }
            }
        }
    }
    @EventHandler
    public void leftClickBlock(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            for (int i = 0; i < getItemEvents().size(); i++) {
                if (event.getItem().getItemMeta().equals(getItemEvents().get(i).getItem().getItemMeta())) {
                    getItemEvents().get(i).leftClickBlock(player);
                }
            }
        }
    }
    @EventHandler
    public void rightClickBlock(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            for (int i = 0; i < getItemEvents().size(); i++) {
                if (event.getItem().getItemMeta().equals(getItemEvents().get(i).getItem().getItemMeta())) {
                    getItemEvents().get(i).rightClickBlock(player);
                }
            }
        }
    }
    public static ArrayList<ItemEvents> getItemEvents() {
        return itemEvents;
    }

}
