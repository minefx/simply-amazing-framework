package com.minefx.simplyamazing.Items;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class ExampleItem2 {
    public static ItemStack ExampleItem2;
    public static void init() {
        ItemBase item = new ItemBase(Material.IRON_INGOT, "GenericName2", 1);
        item.addEnchant(Enchantment.LUCK, 1, true);
        ExampleItem2 = item.outputItem;
    }
}
