package com.minefx.simplyamazing.Items;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public class ExampleItem implements Listener {
    public static ItemStack ExampleItem;
    public static void init() {
        ItemBase item = new ItemBase(Material.ACACIA_FENCE, "GenericName", 1);
        item.addLore("Line 1");
        item.addLore("Line 2");
        item.addLore("Line 3");
        item.addEnchant(Enchantment.LUCK, 1, true);
        item.addItemFlag(ItemFlag.HIDE_ENCHANTS);
        ExampleItem = item.outputItem;
    }
    @EventHandler
    public static void getItem(PlayerInteractEvent event) {
        if (event.getAction().equals(Action.LEFT_CLICK_AIR)) {
            event.getPlayer().getInventory().addItem(ExampleItem);
        }
    }
}
