package com.minefx.simplyamazing.Items;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ItemBase {
    public ItemStack outputItem;
    public ArrayList<String> lore = new ArrayList<String>();
    public ItemBase(Material mat, String displayName, Integer amount) {
        ItemStack item = new ItemStack(mat, amount);
        ItemMeta meta = item.getItemMeta();
        assert meta != null;
        meta.setDisplayName(displayName);
        item.setItemMeta(meta);
        outputItem = item;
    }
    public void editTitle(String title) {
        ItemMeta originalMeta = outputItem.getItemMeta();
        assert originalMeta != null;
        originalMeta.setDisplayName(title);
        outputItem.setItemMeta(originalMeta);
    }
    public void addLore(String line) {
        ItemMeta originalMeta = outputItem.getItemMeta();
        lore.add(line);
        assert originalMeta != null;
        originalMeta.setLore(lore);
        outputItem.setItemMeta(originalMeta);
    }
    public void addEnchant(Enchantment enchant, Integer level, Boolean vanilla) {
        ItemMeta originalMeta = outputItem.getItemMeta();
        assert originalMeta != null;
        originalMeta.addEnchant(enchant, level, vanilla);
        outputItem.setItemMeta(originalMeta);
    }
    public void addItemFlag(ItemFlag flag) {
        ItemMeta originalMeta = outputItem.getItemMeta();
        assert originalMeta != null;
        originalMeta.addItemFlags(flag);
        outputItem.setItemMeta(originalMeta);
    }
}
