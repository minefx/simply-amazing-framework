package com.minefx.simplyamazing;

import com.minefx.simplyamazing.Internal.Init;
import com.minefx.simplyamazing.Items.ExampleItem;
import org.bukkit.plugin.java.JavaPlugin;

public final class Main extends JavaPlugin{
    public String pluginName = "SimplyAmazing";
    // When plugin is enabled, it will initialize.
    @Override
    public void onEnable() {
        Init.enableMessage(pluginName);
        Init.registerEvents(pluginName);
        Init.loadItems(pluginName);
        Init.loadRecipes(pluginName);
        Init.registerCommands(pluginName);
    }
    // When plugin is disabled, it will send a message.
    @Override
    public void onDisable() {
        Init.disableMessage(pluginName);
        Init.unloadRecipes(pluginName);
    }
}