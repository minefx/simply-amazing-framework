package com.minefx.simplyamazing.Recipes;

import com.minefx.simplyamazing.Items.ExampleItem;
import com.minefx.simplyamazing.Recipes.Base.Shapeless;
import org.bukkit.Material;

public class ExampleShapeless {
    public static void init() {
        Shapeless recipe = new Shapeless("example_shapeless", ExampleItem.ExampleItem);
        recipe.addIngredient(Material.GOLD_INGOT);
        recipe.addIngredient(Material.GOLD_BLOCK);
        recipe.registerRecipe();
    }
}
