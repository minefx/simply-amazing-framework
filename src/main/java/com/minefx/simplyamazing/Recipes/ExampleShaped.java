package com.minefx.simplyamazing.Recipes;

import com.minefx.simplyamazing.Items.ExampleItem;
import com.minefx.simplyamazing.Recipes.Base.Shaped;
import org.bukkit.Material;

public class ExampleShaped {
    public static void init() {
        Shaped recipe = new Shaped("example_shaped", ExampleItem.ExampleItem, " L ", "   ", " I ");
        recipe.addIngredient('L', Material.IRON_INGOT);
        recipe.addIngredient('I', Material.DIAMOND);
        recipe.registerRecipe();
    }
}
