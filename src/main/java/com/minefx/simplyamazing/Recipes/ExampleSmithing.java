package com.minefx.simplyamazing.Recipes;

import com.minefx.simplyamazing.Items.ExampleItem;
import com.minefx.simplyamazing.Recipes.Base.Smithing;
import org.bukkit.Material;

public class ExampleSmithing {
    public static void init() {
        Smithing recipe = new Smithing("example_smithing", ExampleItem.ExampleItem, Material.ACACIA_FENCE, Material.ENCHANTED_GOLDEN_APPLE);
        recipe.registerRecipe();
    }
}
