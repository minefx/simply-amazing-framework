package com.minefx.simplyamazing.Recipes;

import com.minefx.simplyamazing.Items.ExampleItem;
import com.minefx.simplyamazing.Recipes.Base.Smoker;
import org.bukkit.Material;

public class ExampleSmoking {
    public static void init() {
        Smoker recipe = new Smoker("example_smoker", ExampleItem.ExampleItem, Material.ENCHANTED_GOLDEN_APPLE, 100, 20);
        recipe.registerRecipe();
    }
}
