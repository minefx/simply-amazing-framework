package com.minefx.simplyamazing.Recipes.Base;

import com.minefx.simplyamazing.Internal.Init;
import com.minefx.simplyamazing.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;

public class Shaped {
    ShapedRecipe outputRecipe;
    public Shaped(String recipeName, ItemStack outputItem, String shape1, String shape2, String shape3) {
        NamespacedKey key = new NamespacedKey(Main.getPlugin(Main.class), recipeName);
        ShapedRecipe recipe = new ShapedRecipe(key, outputItem);
        recipe.shape(shape1,shape2,shape3);
        outputRecipe = recipe;
    }
    public void addIngredient(Character id, Material mat) {
        outputRecipe.setIngredient(id, mat);
    }
    public void registerRecipe() {
        Bukkit.addRecipe(outputRecipe);
    }
}
