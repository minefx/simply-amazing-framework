package com.minefx.simplyamazing.Recipes.Base;

import com.minefx.simplyamazing.Items.ItemBase;
import com.minefx.simplyamazing.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.RecipeChoice;
import org.bukkit.inventory.SmithingRecipe;

public class Smithing {
    SmithingRecipe outputRecipe;
    public Smithing(String recipeName, ItemStack result, Material base, Material addition) {
        NamespacedKey key = new NamespacedKey(Main.getPlugin(Main.class), recipeName);
        SmithingRecipe recipe = new SmithingRecipe(key, result, new RecipeChoice.MaterialChoice(base), new RecipeChoice.MaterialChoice(addition));
        outputRecipe = recipe;
    }
    public void registerRecipe() {
        Bukkit.addRecipe(outputRecipe);
    }
}
