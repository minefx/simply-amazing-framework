package com.minefx.simplyamazing.Recipes.Base;

import com.minefx.simplyamazing.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.RecipeChoice;

public class Smelting {
    FurnaceRecipe outputRecipe;
    public Smelting(String recipeName, ItemStack outputItem, Material source, float exp, int cookTime) {
        NamespacedKey key = new NamespacedKey(Main.getPlugin(Main.class), recipeName);
        FurnaceRecipe recipe = new FurnaceRecipe(key, outputItem, new RecipeChoice.MaterialChoice(source), exp, cookTime);
        outputRecipe = recipe;
    }
    public void registerRecipe() {
        Bukkit.addRecipe(outputRecipe);
    }
}
