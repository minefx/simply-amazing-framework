package com.minefx.simplyamazing.Recipes.Base;

import com.minefx.simplyamazing.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.RecipeChoice;
import org.bukkit.inventory.SmokingRecipe;

public class Smoker {
    SmokingRecipe outputRecipe;
    public Smoker(String recipeName, ItemStack result, Material input, float exp, int cookTime) {
        NamespacedKey key = new NamespacedKey(Main.getPlugin(Main.class), recipeName);
        SmokingRecipe recipe = new SmokingRecipe(key, result, new RecipeChoice.MaterialChoice(input), exp, cookTime);
        outputRecipe = recipe;
    }
    public void registerRecipe() {
        Bukkit.addRecipe(outputRecipe);
    }
}
