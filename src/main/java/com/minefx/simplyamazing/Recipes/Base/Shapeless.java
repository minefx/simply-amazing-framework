package com.minefx.simplyamazing.Recipes.Base;

import com.minefx.simplyamazing.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;

public class Shapeless {
    ShapelessRecipe outputRecipe;
    public Shapeless(String recipeName, ItemStack outputItem) {
        NamespacedKey key = new NamespacedKey(Main.getPlugin(Main.class), recipeName);
        ShapelessRecipe recipe = new ShapelessRecipe(key, outputItem);
        outputRecipe = recipe;
    }
    public void addIngredient(Material mat) {
        outputRecipe.addIngredient(mat);
    }
    public void registerRecipe() {
        Bukkit.addRecipe(outputRecipe);
    }
}
