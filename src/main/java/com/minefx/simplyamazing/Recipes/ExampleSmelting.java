package com.minefx.simplyamazing.Recipes;

import com.minefx.simplyamazing.Items.ExampleItem;
import com.minefx.simplyamazing.Recipes.Base.Smelting;
import org.bukkit.Material;

public class ExampleSmelting {
    public static void init() {
        Smelting recipe = new Smelting("example_smelting", ExampleItem.ExampleItem, Material.ACACIA_LEAVES, 10, 20);
        recipe.registerRecipe();
    }
}
